/* global document localStorage window */
Vue.component('to-do', {
  template: `
    <div>
      <div class="pa4-l">
        <form @submit.prevent="addNewTodo" class="form bg-light-red center b--black-10 br2-ns pa4 mw7">

          <fieldset class="bn cf ma0 pa0 mb3">
            <legend class="mb3 black-80 f4-ns pa0 ttc">title</legend>
              <div class="cf">
                <input v-model="noteTitle" id="titleInput" type="text" placeholder="Give A Title" class="f5-l w-80-l br2-ns pa2 fl bn bg-white">
              </div>
          </fieldset>

          <fieldset class="bn cf ma0 pa0">
            <legend class="mb3 black-80 f4-ns pa0 ttc">note</legend>
              <div class="cf">
                <input v-model="noteBody" id="noteInput" type="text" placeholder="Write Your Note Here" class="f5-l w-80-l br--left-ns br2-ns pa3 fl bn bg-white">
                <input id="submit" type="submit" class="f5-l w-20-l br--right-ns br2-ns pv3 fl bn bg-animate tc white bg-black-70 hover-bg-black pointer">
              </div>
          </fieldset>
        </form>
      </div>
      <ul>
        <li
          is="todo-item"
          v-for="(todo, index) in todos"
          v-bind:key="todo.id"
          v-bind:title="todo.title"
          v-bind:note="todo.note"
          v-on:remove="todos.splice(index, 1)"
          ></li>
      </ul>
    </div>
  `,
  data() {
    return {
      noteTitle: null,
      noteBody: null,
      todos: []
    }
  },
  methods: {
    addNewTodo () {
      this.todos.push({
        title: this.noteTitle,
        note: this.noteBody
      })
      this.noteTitle = null
      this.noteBody = null
    },
  }
})

Vue.component('todo-item', {
  template: '\
    <li class="ml6 mr6 flex items-center bb b--black-10 ph0-l lh-copy bg-washed-blue">\
      <div class="pl4 flex-auto">\
        <h1 class="db black-70">{{ title }}</h1>\
        <p class="db black-70">{{ note }}</p>\
      </div>\
      \
      <div class="mright">\
        <a v-on:click="$emit(\'remove\')" :title="editIcon" class="edit">\
          <i class="fa fa-edit moon-gray hover-black pointer v-mid"></i>\
        </a>\
        <a v-on:click="$emit(\'remove\')" :title="deleteIcon" class="dlt">\
          <i class="mright fa fa-trash-alt moon-gray hover-light-red pointer v-mid"></i>\
        </a>\
        <a :title="completeIcon" class="complete">\
          <i :class="anchorStyle" class="far fa-check-circle moon-gray pointer v-mid"></i>\
        </a>\
      </div>\
    </li>\
  ',
  props: ['title', 'note',],
  data() {
    return {
      deleteIcon: 'Delete',
      editIcon: 'Edit',
      completeIcon: 'Complete',
      anchorStyle: 'hover_green'
    }
  },
  methods: {
    //doSomething() {
    //this.anchorStyle = 'hover-green:focus'
    //}
  }
})

const toDoApp = new Vue ({
  el: '#to-do-app',
  data: {}
})
